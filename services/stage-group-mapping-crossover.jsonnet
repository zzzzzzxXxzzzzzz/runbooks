// This file is autogenerated using scripts/generate_crossover_stage_group_mappings.rb.
// Please update it manually if feature categories were renamed, as:
// {
//   "old_category": "new_category",
// }
//
// Blank values will be ignored.
{
  dependency_scanning: 'software_composition_analysis',
  license_compliance: 'software_composition_analysis',
  advanced_deployments: 'deployment_management',
  kubernetes_management: 'deployment_management',
}
